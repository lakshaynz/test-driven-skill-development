# TDSD - Test Driven Skills Development
#### Applying TDD principles to Human skills development

#### why do I not use flashcards
- truely I could not find a flash cards software that could have higher scope than what it is made for.
- I could really not find a proper goals app lol
- the world caters to the masses's unconsciousness, as skilled professionals, we must also create software that actually helps people, not just give them what they think will help them.
- as an analogy, not only give money to the beggar but to help the beggar realize his own power and teach him to fend for himself and empower him to feel part of the whole, not an isolated rejected fragmant, so that he can update his beliefs to see reality as it actually is , not worse than it actually is.

#### The lose form idea about this app
- more like a chatbot
- the bot asks you questions that are tests
    - these tests must be answerable in a yes or no, along with any details but a yes or no is required.
- the test questions must question the skill level of the human being,
    - for example:
        - Do you know what is "one" in tagalog?
        - Can you do 10 pushups in a row?
        - Can you hold a normal conversation with a native Chinese person?
        - Can you read the xyz book in less than 4 hours?
- The idea is that the questions will be entered by the human using the app, and the questions can have a difficulty level measured from the base line of 0 skill and 0 knowledge about the subject of the question.
- in this way the human can get into the flow of learning a subject because the difficulty can be raised very slowly and the progress can be made swiftly in the skill level of the human.

### Notes
- yes some flashcards apps may be able to do it, I have not found one yet, Ones I found I find them too limiting and not user friendly.
- please reach out to me via the issues of this porject, if there is such an app already
- This app will be made with love for the human using it, to help them achieve their goals.

### want to use this app now?
- please use pen and paper to apply the ideas above to your own choice of skill, make questions that can be answered witha  yes or no, and test yourself until you can answer the questions yes  :)
